

const Page = require('./page');

/**
 * sub page containing specific selectors and methods for a specific page
 */
class PasteBinHome extends Page {
    constructor() {
        super();
        const pasteArea = () => $('#postform-text');
        const pasteExpirationDropDown = () => $("//*[@aria-labelledby='select2-postform-expiration-container']");
        const expirationResultsValues = () => $("#select2-postform-expiration-results");
        const getExpirationOption = (option) => expirationResultsValues().$(`//li[contains(text(),'${option}')]`);
        const pasteName = () => $("#postform-name");
        const createBtn = () => $("//button[contains(text(),'Create New Paste')]");
        const syntaxHighlightingDropDown = () => $("//*[@aria-labelledby='select2-postform-format-container']");
        const syntaxHighlightingResultValues = () => $("#select2-postform-format-results");
        const getSyntaxHighlightingOption = (option) => syntaxHighlightingResultValues().$(`//li[contains(text(),'${option}')]`);
        this.open = () => super.open('https://pastebin.com/');

        this.pastePaste = async (data) => await pasteArea().setValue(data);

        this.setHighlighting = async (option) => {
            await syntaxHighlightingDropDown().scrollIntoView({ block: 'center', inline: 'center' });
            await syntaxHighlightingDropDown().click();
            let optionSelected = await getSyntaxHighlightingOption(option);
            await optionSelected.waitForClickable({ timeout: 5000, interval: 1000 });
            await optionSelected.click();
        }

        this.setExpiration = async (option) => {
            await pasteExpirationDropDown().scrollIntoView({ block: 'center', inline: 'center' });
            await pasteExpirationDropDown().click();           
            let optionSelected = await getExpirationOption(option);
            await optionSelected.waitForClickable({timeout: 5000, interval: 1000 });
            await optionSelected.click();
        }

        this.namePaste = async (name) => await pasteName().setValue(name);
        this.submitCreation = async () => await createBtn().click();
    }
}

module.exports = new PasteBinHome();
