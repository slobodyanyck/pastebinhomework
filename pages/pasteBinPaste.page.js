
const Page = require('./page');

/**
 * sub page containing specific selectors and methods for a specific page
 */
class PasteBinPaste extends Page {
    constructor() {
        super();
        const pastePageHeader = () => $(".content .post-view.js-post-view ");
        const pasteHeader = () => $(".info-bar .info-top>*")
        const highlightingButton = () => $(".highlighted-code .top-buttons .left .btn.h_800[href]");
        const pasteCodeLines = () => $$(".source .li1");

        this.isDisplayed = async () => {
            try {
                await pastePageHeader().waitForDisplayed({
                    timeout: 5000,
                    interval: 1000
                });
                return true;
            }
            catch (e) {
                return false;
            }
        };

        this.getPasteTitle = async () => {
            return await pasteHeader().getText();
        }
        this.getPasteHighlightingType = async () => {
            return await highlightingButton().getText();
        }

        this.getPasteText = async () => {
            let codeLines = await pasteCodeLines();
            let resultCode = await Promise.all(codeLines.map(async line => {
                await line.waitForDisplayed({timeout: 5000, interval: 1000});
                return await line.getText();
            }));
            return resultCode;
        }
    }
}

module.exports = new PasteBinPaste();
