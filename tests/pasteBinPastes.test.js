const pasteBinHomePage = require("../pages/pasteBinHome.page");
const pasteBinPastePage = require("../pages/pasteBinPaste.page");


describe('Create Pastes at Pastebin', () => {
    it('should create new paste', async () => {
        // 1. Open https://pastebin.com or a similar service in any browser
        await pasteBinHomePage.open();
        // 2. Create a New Paste with the following details:
        // * Code: "Hello from WebDriver"
        await pasteBinHomePage.pastePaste("Hello from WebDriver");
        //* Paste Expiration: "10 Minutes"
        await pasteBinHomePage.setExpiration("10 Minutes");
        // * Paste Name / Title: "helloweb"
        await pasteBinHomePage.namePaste("helloweb");

        await pasteBinHomePage.submitCreation();

        expect(await pasteBinPastePage.isDisplayed()).toBe(true);
    });
    it('should create new paste with specific options', async () => {

        //     1. Open https://pastebin.com or a similar service in any browser
        await pasteBinHomePage.open();

        //   2. Create a New Paste with the following details:
        //   * Code:
        const code =
            `git config --global user.name "New Sheriff in Town"
        git reset $ (git commit-tree HEAD ^ {tree} -m "Legacy code")
        git push origin master --force`;

        await pasteBinHomePage.pastePaste(code);

        //* Syntax Highlighting: "Bash"
        const highlighting = "Bash";
        await pasteBinHomePage.setHighlighting(highlighting);

        //* Paste Expiration: "10 Minutes"
        await pasteBinHomePage.setExpiration("10 Minutes");

        //* Paste Name / Title: "how to gain dominance among developers"
        let name = "how to gain dominance among developers";
        await pasteBinHomePage.namePaste(name);

        //3. Save paste and check the following:
        await pasteBinHomePage.submitCreation();

        expect(await pasteBinPastePage.isDisplayed()).toBe(true);

        //* Browser page title matches Paste Name / Title
        expect(browser).toHaveTitle(name);
        expect(await pasteBinPastePage.getPasteTitle()).toContain(name);

        //* Syntax is suspended for bash
        console.log(await pasteBinPastePage.getPasteHighlightingType());
        expect(await pasteBinPastePage.getPasteHighlightingType()).toBe(highlighting);

        //* Check that the code matches the one entered in paragraph 2
        const codeTransformedIntoLines = code.split('\n');
        const codeLinesFromPaste = await pasteBinPastePage.getPasteText();

        codeLinesFromPaste.forEach((line, index) => {
            expect(line).toContain(codeTransformedIntoLines[index]);
        });
    });
});


